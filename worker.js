const axios = require('axios');
var queue = 'queue';
var amqp = require("./amqp");
var maxRetry = 1;
const url = "http://localhost:3000/api";
let channel = null;

function reachMaxRetryHandler(msg){
    console.log("Reach max retry so send out the notification");
}

function timeoutHandler(msg){
    console.error("Timeout handling");
}

function errorHandler(err, msg){
    console.error(error);
}

async function processQueue(err, msg){
    
    if(err){
        errorHandler(err, msg)
    }else if(msg){
        const json = JSON.parse(msg.content.toString());
        console.log(`Process queue:`,json);
        try{
            await axios.get(url, { timeout: 3000 , headers:{
                'Cache-Control': 'no-cache',
                'Pragma': 'no-cache',
                'Expires': '0'
            }});
        }catch(e){
            console.error(e.code);
            if(e.code === 'ECONNABORTED'){
                timeoutHandler(msg);
            }

            if(typeof(json.retry) == "undefined"){
                json.retry = 0;
            }else{
                ++json.retry;
            }
            //reach or over the max retry and simulate the handling
            if(json.retry >= maxRetry){
                reachMaxRetryHandler(json);
            }
            amqp.sendToQueue(queue, JSON.stringify(json));
        }
    }else{
        console.log('no message, waiting');
    }
    setTimeout(function(){
        //notice rabbit mq to complete this task
        if(msg){
            console.log("Done");
            channel.ack(msg);
        }
        consume();
    }, 1e3);
}


async function consume(){
    channel.get(queue, {}, processQueue)
}

async function init(){
    try{
        channel = await amqp.getChannel(queue);
        if(channel){
            consume();
        }else{
            console.error("Channel not found");
        }
    }catch(e){
        console.error(e.stack);
    }
}

init();

// Channel(queue, function(err, channel, conn){
//     if(err){
//         console.error(err.stack);
//     }
//     else{
//         async  function  onConsume(err, msg){
//             console.log("Start onConsume")
//             if(err){
//                 console.warn(err.message);
//             }else if(msg){
//                 //console.log('consuming %j', msg.content.toString());
//                 const json = JSON.parse(msg.content.toString());
                
//                 try{
//                     await axios.get("http://localhost:3000/api");
//                 }catch(e){
//                     if(typeof(json.retry) == "undefined"){
//                         json.retry = 0;
//                     }else{
//                         ++json.retry;
//                     }

//                     //reach or over the max retry and simulate the handling
//                     if(retry >= maxRetry){
//                         reachMaxRetryHandler(json);
//                     }

//                     amqp.sendToQueue(queue, JSON.stringify(json));
//                 }

//                 setTimeout(function(){
//                     channel.ack(msg);
//                     consume();
//                 }, 1e3)
//             }else{
//                 console.log('no message, waiting');
//                 setTimeout(consume, 1e3);
//             }
//         }
//         function consume(){
//             console.log("consume "+queue);
//             channel.get(queue, {}, onConsume);
//         }
//         console.log('channel and queue created');
//         consume();
//     }
// });