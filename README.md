# Background

For any programming, you may need to fire RESTAPI to an third party endpoints and normally you may face problem if the endpoint accidentally failed due to 

* timeout if the process run too long and over the timeout duration so the 
* 5XX problem which some error happened in third party side and break the request

This is a small project which demonstrating using queue to solve this problem.

## Prerequiste

### Install nodejs

Download node js in https://nodejs.org/zh-tw/download/current

### Install package
Go to the project root and run the following command
```
npm i 
```

In this project there are four 

|Package Name|Description|
|---|---|
|amqplib|library for interaction between your code and rabbitmq|
|express|web service for |
|axios|a software can fire web request to web endpoints|
|nodemon|a watcher software, this plugin is used for cater the code changed and immediate apply to |

###  Install docker 
1. Install docker by download at https://www.docker.com/products/docker-desktop/

1. Running the docker service 
   ```
   docker run -p 5672:5672 -p 8080:15672 -d --name=rabbitmq  rabbitmq:3-management
   ```
   the above command is running a docker service and name as rabbitmq and expose 5672 and 15672
   |Name|Port|Description|
   |---|---|---|
   |Rabbit MQ Service|5672|Programming connect this port for sending |
   |Rabbit MQ Management|15672|An admin web portal which you can see the ca|

1. For rabbit mq connection will use the following code to connect (in my demo will using 5672)
    ```
    const url = 'amqp://guest:guest@localhost:5672';
    ampq.connect(url, (err, conn)=>{
        if(err){
            reject(err)
        }else{
            resolve(conn);
        }
    });
    ```
1. During development you have to monitor whether you trigger adding data to the queue and the data as the result you can access http://localhost:8080 in this tutorial to access the rabbitmq management (due to the port 8080 is mapping to the docker 15672 in this tutorial)
   ![Rabbit management](./screenshots/rabbitmq-management.png "Rabbit management")
   ![Queue management](./screenshots/queue-management.png "Queue management")
   and username is __guest__ and password is __guest__ by default to login
   ![Login](./screenshots/login.png "Login")

## Files 

### index.js

All the endpoints are located in index.js and they are as follow:
|api|description|
|---|---|
|localhost:3000|Our web endpoint to trigger demo on adding data to message to queue|
|localhost:3000/api|Simulate a third party api and may have fail or timeout|

Execute
```
npx nodemon index.js
```

### worker.js

this is a worker for listening whether the queue got message to process and if there is, will fire an api url http://localhost:3000/api and accord to the return status to trigger the errorhandler or timeouthandler or successhandler.
and the rate will be accord set in index.js

Execute
```
npx nodemon worker.js
```

### amqp.js
This is the utils function I wrote for connecting to the rabbit mq

## Simulate Steps

1. First login the rabbitmq management in https://localhost:8080, in this sample will go http://localhost:8080/#/queues/%2F/queue
   ![Queue](./screenshots/queue.png "Queue")
   when you scroll down a bit, you should able to see the above screen and please try to click get message, and at this moment you should see no message at this moment
1. Then please access http://localhost:3000 for inserting a message to the queue and retry the step 1 and you should able to see a new message created as below
   ![Queue](./screenshots/get-message.png "Queue")
1. Then please run Terminal or Command Prompt and go to the project root, then run the following command
    ```
    npx nodemon worker.js
    ```
    The worker.js will start working and making an ajax call to localhost:3000/api, you may see the following screen
    ![Worker](./screenshots/worker.png "worker")

    |Name|Description|
    |---|---|
    |ERR_BAD_RESPONSE|ajax got non 2XX response and trigger failure hander which increase the retry and add the message to same queue for retrying |
    |ECONNABORTED|ajax got timeout and trigger the timeout handling|