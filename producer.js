var Channel = require('./channel');
var queue = 'queue';

Channel(queue, async function(err, channel, conn){
    if(err){
        console.error(err.stack);
    }else{
        //var work = {abc:"this is for testing"};
        var work = "hello";
        // channel.assertQueue(queue, {
        //     durable: true
        // });
        const response = await channel.sendToQueue(queue, encode(work), {
            persistent: true
        });
        console.log(response);

        setImmediate(async function(){
            try{
                console.log("start close connection");
                await channel.close();
                await conn.close();
                console.log("close connection");
            }catch(e){
                console.error(e);
            }
        })
    }
});

function encode(json){
    return Buffer.from(JSON.stringify(json), "utf-8");
}