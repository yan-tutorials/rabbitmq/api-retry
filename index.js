const express = require("express");
const axios = require('axios');
const q = require("./amqp");
const app = express();
//can defined by your ideal ports
const port = 3000;

app.get("/", async(req, res)=>{
    const date = new Date().toUTCString();
    const body = { abc:"abc", time:date };
    let message = "Hello";
    let statusCode = 200;

    try{
        const ok = await q.sendToQueue("queue", JSON.stringify({data:message, retry: 0}));
        console.log("OK",ok);

    }catch(e){
        console.error(e);
        message = e.message;
        statusCode = 500;
    }
    res.status(statusCode).json({message:message, body:body});
    
});

app.get("/api", async(req,res)=>{
    const random = Math.floor(Math.random() * 1000);
    console.log(random);
    const statusCode = (random % 5  == 1) ? 200:500;

    const isTimeout =  (random % 8  == 1);
    if(isTimeout){
        setTimeout(()=>{
            res.status(statusCode).send('');
        }, 5e3);
    }else{
        res.status(statusCode).send('');
    }
    
});

app.listen(port);


