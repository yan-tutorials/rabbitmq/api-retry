const ampq = require('amqplib/callback_api');
const url = process.env.AMQP_URL || 'amqp://guest:guest@localhost:5672';

function createQueueChannel(queue, cb){
    return new Promise(resolve, reject =>{
        try{
            ampq.connect(url, (err, conn)=>{
                if(err){
                    reject(err);
                }else{
                    resolve(conn);
                }
            });
        }catch(e){
            reject(err)
        }
    })    
}

function createChannel(conn){
    return new Promise((resolve, reject)=>{
        conn.createChannel((err, channel) => {
            if(err){
                reject(err)
            }else{
                resolve(channel);
            }
        });
    });
}

function createQueue(queue, channel){
    return new Promise((resolve, reject)=>{
        // console.log(`Assert Queue: ${queue}`);
        channel.assertQueue(queue, {durable: true}, (err)=>{
            if(err){
                reject(err);
            }else{
                resolve(true);
            }
        });
    });
}

async function init(){
    return new Promise((resolve, reject)=>{
        ampq.connect(url, (err, conn)=>{
            if(err){
                reject(err)
            }else{
                resolve(conn);
            }
        });
    })
}

async function getChannel(topic){    
    try{
        conn = await init();
        //console.log("Conn",conn);
        return await createChannel(conn);
    
    }catch(e){
        console.error(e.message);
        return null;
    }
}

async function sendToQueue(topic, data){
    let conn = null, channel = null;
    try{
        conn = await init();
        //console.log("Conn",conn);
        channel = await createChannel(conn);
        //console.log("Channel",channel);
        const createdQueue = await createQueue(topic, channel);
        if(createdQueue){
            console.log("here");
            console.log("Before send", topic, data);
            const response = await channel.sendToQueue(topic, Buffer.from(data), {persistent:false});
            console.log(response);
            console.log("after send");
        }
        return true;        

    }catch(e){
        console.error(e);
        return false;
    }finally{
        console.log("finally close");
        setTimeout(async ()=>{
            if(channel){
                await channel.close();
            }
            if(conn){
                await conn.close();
            }
        },1e1);
    }
}

module.exports = {
    sendToQueue,
    getChannel
}